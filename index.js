/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function personalInfo(){
	alert("Hello, Human!");
	let usersFirstName = prompt("What is your name? ");
	let usersAge = prompt("What is your age? ")
	let usersAddress = prompt("Where do you live? ")

	console.log("Hello, " + usersFirstName);
	console.log("Your are " + usersAge + " years old. ");
	console.log("You live in " + usersAddress);
}
personalInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function faveMusicBand(){
	let musicBand1 = "The Beatles";
	let musicBand2 = "Metallica";
	let musicBand3 = "The Eagles";
	let musicBand4 = "L'arc~en~Ciel";
	let musicBand5 = "Eraserheads";

	console.log("1. " + musicBand1);
	console.log("2. " + musicBand2);
	console.log("3. " + musicBand3);
	console.log("4. " + musicBand4);
	console.log("5. " + musicBand5);
}
faveMusicBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function faveMovies(){
console.log("1. The Godfather ");
console.log("Rotten Tomatoes Rating: 97%");
console.log("2. The Godfather, Part II ");
console.log("Rotten Tomatoes Rating: 96%");
console.log("3. Shawshank Redemption ");
console.log("Rotten Tomatoes Rating: 91%");
console.log("4. To Kill a Mockingbird ");
console.log("Rotten Tomatoes Rating: 93%");
console.log("5. Psycho ");
console.log("Rotten Tomatoes Rating: 96%");

}
faveMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}
printUsers()


